exec { 'apt-update':
  path    => '/usr/bin',
  command => 'sudo apt-get update; return 0;',
  require => [File['php-deb'], Exec['mysql-source']],
}

package { 'libapache2-mod-fastcgi':
  ensure          => 'latest',
#  install_options => '--force-yes',
  require         => [ Exec['apt-update'], Package['apache2']],
}

package { 'php5-fpm':
  ensure  => '5.6.*',
  require => [Package['libapache2-mod-fastcgi'], File['php-deb']],
}

package { 'php5-cli':
  ensure => 'latest',
  require => Package['php5-fpm'],
}

package { ['php5-gd', 'php5-curl', 'php5-ldap', 'php5-xdebug']:
  ensure => 'latest',
  require => Package['php5-fpm'],
}

package { ['freetds-common', 'freetds-bin',  'unixodbc', 'php5-mssql']:
  ensure  => 'latest',
  require => Package['php5-fpm'],
}

package { 'php5-mysql':
  ensure => '5.6.*',
  require => Package['php5-fpm'],
}

exec { 'mysql-source':
  command => "/usr/bin/wget -O /tmp/mysql-apt-config_0.2.1-1debian7_all.deb http://repo.mysql.com/mysql-apt-config_0.2.1-1debian7_all.deb && /usr/bin/dpkg -i /tmp/mysql-apt-config_0.2.1-1debian7_all.deb",
  require => File['php-deb']
}

package { 'apache2':
  ensure  => '2.2.*',
  require => [Exec['apt-update']],
}

file { 'php-deb':
  ensure  => 'present',
  path => '/etc/apt/sources.list.d/dotdeb.list',
  source  => '/vagrant/debsources/dotdeb.list',
  require => Exec['dotdeb-gpg']
}

exec { 'dotdeb-gpg':
  command => '/usr/bin/wget http://www.dotdeb.org/dotdeb.gpg -O /tmp/dotdeb.gpg && /usr/bin/apt-key add /tmp/dotdeb.gpg',
}

## CasperJS
exec { 'nodejs-source':
  command => '/usr/bin/curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -',
  notify => Exec['apt-update']
}

package { ['nodejs', 'build-essential', 'g++', 'flex', 'bison', 'gperf', 'ruby', 'perl', 'libsqlite3-dev', 'libfontconfig1-dev', 'libicu-dev', 'libfreetype6', 'libssl-dev',
  'libpng-dev', 'libjpeg-dev', 'python', 'libx11-dev', 'libxext-dev']:
  ensure => 'latest',
  require => Exec['nodejs-source'],
  notify => Exec['phantomjs']
}

exec { 'casperjs':
  command => '/usr/bin/npm install -g casperjs',
  require => Package['nodejs']
}

exec { 'phantomjs':
  environment => ['PHANTOM_JS=phantomjs-1.9.8-linux-x86_64'],
  command => '/usr/bin/wget https://bitbucket.org/ariya/phantomjs/downloads/$PHANTOM_JS.tar.bz2; /bin/tar xvjf $PHANTOM_JS.tar.bz2; sudo /bin/mv $PHANTOM_JS /usr/local/share; sudo /bin/ln -sf /usr/local/share/$PHANTOM_JS/bin/phantomjs /usr/local/bin'
}
## /CasperJS

class { '::mysql::server':
  root_password    => 'UNSET',
  override_options => {
    'mysqld' => {
      'max_connections' => '1024',
      'datadir' => '/var/lib/mysql',
      'bind_address' => '0.0.0.0'
    }
  },
  require => [Exec['apt-update', 'mysql-source']],
  notify  => Exec['db-stop-manual'],
  package_ensure => '5.6.*',
}

exec { 'db-stop-manual':
  command => '/usr/bin/mysqladmin shutdown',
  notify => Exec['db-restart']
}

exec { 'db-restart':
  command => '/etc/init.d/mysql restart'
}

file { '/etc/apache2/conf.d/php5-fpm.conf':
  ensure => 'file',
  content => template('/vagrant/templates/php5-fpm.conf'),
  mode => 644,
  require => [Package['php5-fpm'], Exec['a2enmod-actions']],
  notify => Exec['apache-restart', 'fpm-restart'],
}

file { '/etc/php5/fpm/php.ini':
  ensure => 'file',
  content => template('/vagrant/templates/php.ini'),
  mode => 644,
  require => [Package['php5-fpm']],
  notify => Exec['fpm-restart'],
}

exec { 'a2enmod-rewrite':
  command=> '/usr/sbin/a2enmod rewrite',
  require => Package['apache2'],
}

exec { 'a2enmod-actions':
  command=> '/usr/sbin/a2enmod actions',
  require => Package['libapache2-mod-fastcgi', 'apache2'],
}

exec { 'apache-restart':
  command => '/etc/init.d/apache2 restart'
}

exec { 'fpm-restart':
  command => '/etc/init.d/php5-fpm restart'
}

package {['curl', 'git']:
  ensure => 'latest'
}

exec { 'composer':
  command => '/usr/bin/curl -s http://getcomposer.org/installer -o /tmp/composer;  sudo /usr/bin/php /tmp/composer --install-dir=/usr/local/bin --filename=composer',
  require => Package['php5-cli', 'curl']
}
