# Vagrant up :-)

## Install VirtualBox

[Download page](https://www.virtualbox.org/wiki/Downloads)

## Install vagrant

[Download page](https://www.vagrantup.com/downloads.html)

## Clone this repo :-)
Dir with this repo is working dir for Vagrant. You will run tasks like "vagrant up" from there.

## Download box
Open command line / terminal anywhere and run folowing command
 
    # vagrant box add --insecure debian77 https://eshare.wunderman.com/openpublicdocument.do?link_id=ZSkCIvo192&entryId=c7cef565-30eb-4c3d-bd3b-9ac26c537168
  

Create new file "Vagrantfile" from Vagrantfile.example <br>
Open Vagrantfile and set-up following

# Configuration of Vagrantfile

## Required configuration

	config.vm.synced_folder "/path/to/your/projects", "/vagrant/sources"
	
*Tip: For better performance of synced folders you can use following*

#### Mac

	config.vm.synced_folder "/path/to/your/projects", "/vagrant/sources", type: "nfs"

#### Windows
  [Windows settings] (/docs/windows.md)

## Optional configuration 

	config.vm.network "private_network", ip: "192.168.50.50"
  
# Vagrant basics

## First run Vagrant box

Open command line / terminal in root folder of this repository and run

	vagrant up

## Restart virtual

	vagrant reload

or 

	vagrant reload --provision

for restart and run provisioner	

## SSH to virtual

	vagrant ssh

## Destroy virtual

	vagrant destroy

## Run provisioner

	vagrant provision

## More info

See [Vagrant domentation](https://docs.vagrantup.com/v2/getting-started/index.html)

# Creating vhost for your project

Copy and modify vhosts/000-webtools

Root of your projects folder is by default **/home/vagrant/sources**

*Tip: 000 on beginning of file determines the priority of vhost*

To your hosts insert following line (modified by vhost name)

	192.168.50.50 adminer.dev
	
	
# Troubleshoots
	
I have created vhost and I see "It works!" if I open the page<br>
**You must restart Apache server**

	vagrant ssh
	sudo service apache2 restart

Vagrant don´t start properly and in Vhost directory is weird file 000-Default, that can´t be open.
**You must delete this file and reload vagrant machine.**