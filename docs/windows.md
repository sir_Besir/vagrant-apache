# Windows Config

## For SSH connection install SSH executable - usually part of GIT or Cygwin installer
[Download page](https://git-scm.com/download/)

## In windows 7 install latest PowerShell (Windows Management Framework 4.0)
[Download page](https://www.microsoft.com/en-us/download/details.aspx?id=40855)

## Setting up maping of directories to vagrantbox, thru samba protocol (best for windows)         
### 1. Set up new user (eg. vagrant) with pasword (eg. 'Kreslo123') and give him to groups Administrators and SophosAdministrator 
*Tip: As username you can also use your domain account in format "user@domain".     
### 2. Open RegEdit and find 'HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management' and set key LargeSystemCache=3. 
### 3. Open as Administrator 'cmd' and run command 'NET USE /PERSISTENT:YES'
### 4. Create as USER new directories c:\www, c:\mysql 
*Tip: Also you can use own directory structure, but don not remmeber to proprietally update synced folder lines in your vagrant file. Yes, you can use different directories for your web and DB data, but don`t ask me for help.     
### 5. Restart windows, brain and word.
### 6. Add to vagrantfile this lines
#### Vhost directory
    config.vm.synced_folder "./vhosts", "/etc/apache2/sites-enabled", type: "smb", smb_host:"192.168.50.1", smb_username:"vagrant", smb_password:"Kreslo123" 

#### DB directory
    config.vm.synced_folder "c:/mysql", "/var/lib/mysql", owner: "mysql", group: "mysql", mount_options: ['dmode=775', 'fmode=775'], smb_host:"192.168.50.1", type: "smb", smb_username:"vagrant", smb_password:"Kreslo123"

#### Sites directory
    config.vm.synced_folder "c:/www", "/home/vagrant/sources", owner: "www-data", group: "www-data", mount_options: ['dmode=775', 'fmode=775'], type: "smb", smb_host:"192.168.50.1", smb_username:"vagrant", smb_password:"Kreslo123" 

*Tip: Add to your Vagrant file a section of network definitions (use 82540EM as net interface), for dodge of huge network latency.
              
## Performance configuration   
### For low performance of virtualization, you can set virtual box provider settings. It´s mostly for windows users.
*Tip: For more than one CPU core, you must have enabled CPU Virtualization.
  
    config.vm.provider "virtualbox" do |v|
        v.customize ["modifyvm", :id, "--nictype1", "82540EM"]
        v.customize ["modifyvm", :id, "--nictype2", "82540EM"] 
	      v.customize ["modifyvm", :id, "--ioapic", "on"  ]
        v.customize ["modifyvm", :id, "--cpus"  , "2"   ]
        v.customize ["modifyvm", :id, "--memory", "512"]
		v.customize ["modifyvm", :id, "--pae", "on"]
    end